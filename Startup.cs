﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace PlanLekcji
{
  public class Startup
  {
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc();

      services.AddSwaggerGen(config =>
          config.SwaggerDoc("v1", new Info { Title = "Plan Lekcji Web API", Version = "v1" })
      );
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      // SPA fallback routes
      app.Use(async (context, next) =>
      {
          if(!Path.HasExtension(context.Request.Path.Value)
              && !context.Request.Path.StartsWithSegments(new PathString("/api")))
          {
              context.Request.Path = "/index.html";
              await next();
          }
          else
              await next();
      });

      app.UseCors(config => 
          config.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()
      );

      app.UseDefaultFiles()
          .UseStaticFiles()
          .UseMvc();

      app.UseSwagger();
      app.UseSwaggerUI(config => 
          config.SwaggerEndpoint("/swagger/v1/swagger.json", "Plan Lekcji Web API")
      );
    }
  }
}
