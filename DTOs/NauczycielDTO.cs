namespace PlanLekcji
{
  public class NauczycielDTO
  {
    public long Id { get; set; }
    public string Imie { get; set; }
    public string Nazwisko { get; set; }
    public string TelKontaktowy { get; set; }
  }
}