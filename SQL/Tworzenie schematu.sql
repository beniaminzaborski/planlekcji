
USE PlanLekcji
GO

/*

DROP TABLE Uczniowie;
DROP TABLE Klasy;
DROP TABLE Przedmioty;
DROP TABLE Nauczyciele;

*/

CREATE TABLE Nauczyciele (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  Imie VARCHAR(50) NOT NULL,
  Nazwisko VARCHAR(100) NOT NULL,
  TelKontaktowy VARCHAR(12)
);

CREATE TABLE Przedmioty (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  Nazwa VARCHAR(100) NOT NULL
);

CREATE TABLE NauczycielePrzedmioty (
  NauczycielId BIGINT NOT NULL REFERENCES Nauczyciele(Id),
  PrzedmiotId BIGINT NOT NULL REFERENCES Przedmioty(Id),
  CONSTRAINT PK_NauczycielePrzedmioty PRIMARY KEY (NauczycielId, PrzedmiotId)
);

CREATE TABLE Klasy (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  Nazwa VARCHAR(100) NOT NULL,
  Rocznik TINYINT NOT NULL,
  WychowawcaId BIGINT NOT NULL REFERENCES Nauczyciele(Id)
);

CREATE TABLE Uczniowie (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  Imie VARCHAR(50) NOT NULL,
  Nazwisko VARCHAR(100) NOT NULL,
  AdresZamieszkania VARCHAR(MAX) NOT NULL,
  TelKontaktowy VARCHAR(12),
  KlasaId BIGINT NOT NULL REFERENCES Klasy(Id)
);

CREATE TABLE PlanLekcji (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  KlasaId BIGINT NOT NULL REFERENCES Klasy(Id)
);

CREATE TABLE PozycjaPlanuLekcji (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  PlanLekcjiId BIGINT NOT NULL REFERENCES PlanLekcji(Id),
  PrzedmiotId BIGINT NOT NULL REFERENCES Przedmioty(Id),
  NauczycielId BIGINT NOT NULL REFERENCES Nauczyciele(Id),
  DzienTygodnia TINYINT NOT NULL,
  GODZINA TINYINT NOT NULL,
  SALA VARCHAR(4) NOT NULL
);

CREATE TABLE Sale (
  Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
  Nazwa VARCHAR(4) NOT NULL
);

ALTER TABLE PozycjaPlanuLekcji DROP COLUMN GODZINA;
ALTER TABLE PozycjaPlanuLekcji ADD Godzina TINYINT NOT NULL;

ALTER TABLE PozycjaPlanuLekcji DROP COLUMN SALA;
ALTER TABLE PozycjaPlanuLekcji ADD SalaId BIGINT NOT NULL REFERENCES Sale(Id);


INSERT INTO Przedmioty (
  Nazwa
) VALUES (
  'Informatyka'
);
INSERT INTO Przedmioty (
  Nazwa
) VALUES (
  'Matematyka'
);
INSERT INTO Przedmioty (
  Nazwa
) VALUES (
  'Historia'
);

SELECT * FROM Przedmioty;

INSERT INTO Nauczyciele(
  Imie, Nazwisko, TelKontaktowy
) VALUES (
  'Jan', 'Kowalski', '+48888000111'
);
INSERT INTO Nauczyciele(
  Imie, Nazwisko, TelKontaktowy
) VALUES (
  'Janina', 'Nowak', '+48888000222'
);
INSERT INTO Nauczyciele(
  Imie, Nazwisko, TelKontaktowy
) VALUES (
  'Roman', 'Janowski', '+48888000333'
);

select Nazwisko, TelKontaktowy from Nauczyciele;

INSERT INTO NauczycielePrzedmioty VALUES (1, 1);
INSERT INTO NauczycielePrzedmioty VALUES (1, 2);

INSERT INTO NauczycielePrzedmioty VALUES (2, 2);

INSERT INTO NauczycielePrzedmioty VALUES (3, 3);

SELECT * FROM NauczycielePrzedmioty;


SELECT n.Imie, n.Nazwisko, p.Nazwa FROM NauczycielePrzedmioty np
INNER JOIN Nauczyciele n ON n.Id = np.NauczycielId
INNER JOIN Przedmioty p ON p.Id = np.PrzedmiotId;

SELECT Imie, Nazwisko, Nazwa FROM Nauczyciele CROSS JOIN Przedmioty;

DELETE FROM NauczycielePrzedmioty WHERE NauczycielId = 3;

UPDATE Nauczyciele SET
  TelKontaktowy = '777000221'
WHERE Nazwisko = 'Nowak';

ALTER TABLE Klasy ALTER COLUMN Rocznik SMALLINT NOT NULL;

INSERT INTO Klasy (
  Nazwa, Rocznik, WychowawcaId
) VALUES (
  '3a', 2018, 1
);
INSERT INTO Klasy (
  Nazwa, Rocznik, WychowawcaId
) VALUES (
  '3b', 2018, 2
);


select * from Klasy

select n.Imie, n.Nazwisko, k.Nazwa from Nauczyciele n
left outer join Klasy k on k.WychowawcaId = n.Id;

update Nauczyciele set TelKontaktowy = NULL where Nazwisko = 'Nowak'

select * from Nauczyciele where TelKontaktowy IS NULL;

update Nauczyciele set Imie = NULL where Nazwisko = 'Nowak';

