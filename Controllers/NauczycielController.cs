using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace PlanLekcji
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class NauczycielController : Controller
  {
    private readonly IConfiguration configuration;

    public NauczycielController(IConfiguration configuration)
    {
      this.configuration = configuration;
    }

    [HttpGet]
    public IEnumerable<NauczycielDTO> GetAll()
    {
      List<NauczycielDTO> nauczyciele = new List<NauczycielDTO>();  
    
      using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
      {  
        SqlCommand command = new SqlCommand("SELECT Id, Imie, Nazwisko, TelKontaktowy FROM Nauczyciele", connection);
        command.CommandType = CommandType.Text;

        connection.Open();
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {  
            NauczycielDTO nauczyciel = new NauczycielDTO();

            nauczyciel.Id = reader.GetInt64(0);
            nauczyciel.Imie = reader.GetString(1);
            nauczyciel.Nazwisko = reader.GetString(2);
            nauczyciel.TelKontaktowy = reader.GetString(3); 

            nauczyciele.Add(nauczyciel);
        }  
        connection.Close();
      }

      return nauczyciele;
    }
  }
}